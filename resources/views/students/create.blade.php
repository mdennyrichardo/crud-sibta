<!-- resources/views/students/create.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Mahasiswa Baru</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('students.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="nama">Nama:</label>
                            <input type="text" name="nama" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="nim">NIM:</label>
                            <input type="text" name="nim" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="fakultas">Fakultas:</label>
                            <input type="text" name="fakultas" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="jurusan">Jurusan:</label>
                            <input type="text" name="jurusan" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="nomor_telepon">Nomor Telepon:</label>
                            <input type="text" name="nomor_telepon" class="form-control" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection